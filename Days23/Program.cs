﻿

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Metrics;

namespace Days2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Pilih menu materi:\n" +
                "1. Arrays\n" +
                "2. Collections\n" +
                "3. Hashtable\n" +
                "4. Dictionary\n" +
                "5. List\n" +
                "Pilih: ");
                String choice = Console.ReadLine();

                if (choice == "1")
                {
                    arrays();
                }
                else if (choice == "2")
                {
                    collections();
                }
                else if (choice == "3")
                {
                    hashtable();
                }
                else if (choice == "4")
                {
                    dictionary();
                }
                else if (choice == "5")
                {
                    list();
                }
                else
                {
                    Console.WriteLine("Pilihan tidak ditemukan");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        static void arrays ()
        {
            string[] cars = { "Volvo", "BMW", "Ford", "Mazda" };
            Console.WriteLine(cars.Length);
            foreach (string car in cars)
            {
                Console.WriteLine(car);
            }
        }

        static void collections ()
        {
            var warna = new List<string>();
            warna.Add("Hitam");
            warna.Add("Coklat");
            warna.Add("Biru");
            warna.Add("Putih");

            foreach (var data in warna)
            {
                Console.Write(data + " ");
            }

            warna.Remove("Biru");

            Console.WriteLine("\n\nAfter remove\n");

            foreach (var data in warna)
            {
                Console.Write(data + " ");
            }
        }

        static void hashtable()
        {
            var countries = new Hashtable(){
                {"USA", "Amerika Serikat"},
                {"PHL", "Filipina"},
                {"IDN", "Indonesia"},
                {"IND", "India"}
            };

            foreach (DictionaryEntry data in countries) { 
                Console.WriteLine("Key: {0}, Value: {1}", data.Key, data.Value);
            }

            countries["IDN"] = "Indonesia (Update)";

            Console.WriteLine("\n\nAfter update\n");

            foreach (DictionaryEntry data in countries)
            {
                Console.WriteLine("Key: {0}, Value: {1}", data.Key, data.Value);
            }
        }

        static void dictionary()
        {
            var countries = new Dictionary<string, string>(){
                {"USA", "Amerika Serikat"},
                {"PHL", "Filipina"},
                {"IDN", "Indonesia"},
                {"IND", "India"}
            };

            foreach (var data in countries)
            {
                Console.WriteLine("Key: {0}, Value: {1}", data.Key, data.Value);
            }

            countries["IDN"] = "Indonesia (Update)";

            Console.WriteLine("\n\nAfter update\n");

            foreach (var data in countries)
            {
                Console.WriteLine("Key: {0}, Value: {1}", data.Key, data.Value);
            }
        }

        static void list()
        {
            // Integer
            List<int> angka = new List<int>();
            angka.Add(2);
            angka.Add(3);
            angka.Add(4);
            angka.Add(5);

            angka.Remove(3);

            var cities = new List<string>();
            cities.Add("Bekasi");
            cities.Add("Bandung");
            cities.Add("Purwokerto");
            cities.Add("Jakarta");
            cities.Remove("Jakarta");

            foreach (var data in angka)
            {
                Console.Write(data + " ");
            }

            Console.Write("\n\n");

            foreach (var data in cities)
            {
                Console.Write(data + " ");
            }
        }


    }
}